// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const git = require('simple-git');
const path = require('path');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    //console.log('Congratulations, your extension "gitacora" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    vscode.workspace.onDidSaveTextDocument((document) => {
        const currentFileDir = path.dirname(document.fileName);
        const repository = git(currentFileDir);

        repository.checkIsRepo(function(err, isRepo) {
            if (isRepo) {
                repository.diffSummary((err, status) => {
                    const numberOfFilesChanged = status.files.length

                    if (numberOfFilesChanged > 0){
                        repository.add(document.fileName).commit(`Gitacora saved ${numberOfFilesChanged} changed files`);
                        repository.getRemotes(function(err, remotes) {
                            const validRemotes = remotes.filter((remote) => remote.name != '');
                            if(validRemotes.length > 0) {
                                repository.push(['origin', 'master'], () => {})
                            }
                        });
                    }
                })
            }
        });

    }, this);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;