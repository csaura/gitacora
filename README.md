# Gitacora

Gitacora is a very simple extension to record and store file changes with git. 

## Features

When a file in a git repository is saved, commit and push to origin remote.

If the file is not a in a git repository the extension does not try to commit.

If the file is in a git repository but does not have remotes, only commit.


## Requirements

You need to have `git` installed in your computer.

## Extension Settings

There are no setting in this early stage, but is something that will come in more mature versions


## Release Notes

This extension is in its very early stages, but working.

### 0.0.1

Initial release with the minium. Just work. After save the file, generates the commit and push to origin master.

-----------------------------------------------------------------------------------------------------------

## Working with Markdown

**Note:** there are many aplications and options to increase the functionality of the extension. Proposals are welcome, but simplicity and easy to use is at the top of the values.

**Enjoy Gitacora!**